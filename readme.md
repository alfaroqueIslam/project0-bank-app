# Project 0

### Driver and Service Classes:
ActionsDriver- This is the class that holds the main method and executes the main sequence of the program.

MenuDriver- Executes the menu code for the user and provides options based on whether they are a customer or employee.

LoginService- Takes an email and password from user input.

LoginDriver- Stores the user's email and password to later be authenticated.

### Dao Classes:
AuthenticationDaos- Takes user credentials from a LoginDriver and queries the database to authenticate.

AccountDaos- Contains all the funcionality for communicating with the Account table in the database including methods to create a new account, depositing and withdrawing funds and view account data.

TransactionDaos- Records withdrawals and deposits to then add them to the Account_Transaction table in the database, it also provides a method to view all transactions in that table.

### Model Classes:
User- This is an abstract class that holds user data and extends to the Customer, Employee and Manager classes.

Account- This class is used to hold account data.

### Database:

![alt text](project0_db.PNG "Bank Data")


